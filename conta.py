class Conta:

    def __init__(self, numero, titular, saldo, limite):
        self.__numero = numero
        self.__titular = titular
        self.__saldo = saldo
        self.__limite = limite

    @property
    def numero(self):
        return self.__numero

    @property
    def titular(self):
        return self.__titular

    @property
    def saldo(self):
        return self.__saldo

    @property
    def limite(self):
        return self.__limite

    @numero.setter
    def numero(self, numero):
        self.__numero = numero

    @titular.setter
    def titular(self, titular):
        self.__titular = titular

    @saldo.setter
    def saldo(self, saldo):
        self.__saldo = saldo

    @limite.setter
    def limite(self, limite):
        self.__limite = limite

    def extrato(self):
        print("O SALDO DO {} É R$ {}".format(self.__titular, self.__saldo))

    def deposita(self, valor):
        self.__saldo += valor

    def __permite_saque(self, valor):
        if (valor <= (self.__saldo + self.__limite)):
            return True
        else:
            return False

    def saque(self, valor):
        if (self.__permite_saque(valor)):
            self.__saldo -= valor
        else:
            print("SALDO INSUFICIENTE, SEU SALDO É R$ {}".format(self.__saldo + self.__limite))

    def transfere(self, valor, conta_destino):
        try:
            if self.__saldo >= valor:
                self.__saque(valor)
                conta_destino.deposita(valor)
            else:
                print("SALDO INSUFICIENTE, SEU SALDO É R$ {}".format(self.__saldo))
        except ValueError:
            print("ERROR")

    @staticmethod
    def cod_banco():
        return "001"

    @staticmethod
    def cod_bancos():
        return {"001":"Banco do Brasil", "237":"Bradesco"}



